FROM debian

RUN apt-get -y update;\
    apt-get -y upgrade;\
	apt-get -y install curl wget git mercurial procps;\
	apt-get -y install ruby ;\
    apt-get -y clean; \
    rm -rf /var/lib/apt/lists/*


RUN gem install sinatra
ADD caddy /usr/local/bin

WORKDIR /root

ADD Caddyfile /root
ADD assets /root/assets
ADD start.sh /root
ADD updater.rb /root
ADD webhook.sh /root
ADD wiki.html /root
ADD index.html /root

EXPOSE 8080 4567
CMD ["./start.sh"]
