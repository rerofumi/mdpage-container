httpd サーバーとして caddy を使います。
予めダウンロードして実行ファイル caddy をこのディレクトリに置いてください。

caddy は以下から入手できます。利用ライセンスに従ってください。
https://caddyserver.com/download
https://github.com/mholt/caddy/releases

