# markdown base WEB server container

## Description

gitリポジトリに置かれた markdown file を WEBサイトとして公開する docker container を構築します。


## Install

1. `docker build -t (container name) .`

## Execute

- `docker run -d -e "GITREPOS=(contents repository url)" -p "8080:8080" -p "4567:4567" (container name)`

起動時に GITREPOS でリポジトリURLを指定することで、そのリポジトリを `md/` 以下に展開します。

このコンテンツリポジトリは markdown file が登録されているリポジトリで、この markdown file 自体が WEB サイトを構成します。

port 8080 は WEBサイトを表示する port です。

port 4567 はコンテンツの更新を行うための port です。`http://(url):4567/hook` を何らかの形で GET すると、GITREPOS で指定されたリポジトリに対し pull を行います。

コンテンツリポジトリの webhook をこの更新 port に向けると、コンテンツリポジトリに対して push 更新を行うと同時に WEBサイトに反映される様になります。



## Contribution

1. Fork ([https://bitbucket.org/rerofumi/mdpage-container/fork](https://bitbucket.org/rerofumi/mdpage-container/fork))
1. Create a feature branch
1. Commit your changes
1. Rebase your local changes against the master branch
1. Create a new Pull Request

## Author

[rerofumi](https://bitbucket.org/rerofumi)
